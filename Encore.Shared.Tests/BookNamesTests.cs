﻿using Encore.Shared;
using System;
using Xunit;
using System.Collections.Generic;
using Xunit.Abstractions;
using Shouldly;

namespace Encore.Shared.Tests
{
    public class BookNamesTests
    {
        private readonly ITestOutputHelper output;

        public BookNamesTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void ShouldGetParatext()
        {
            var myList = new List<string>(BookNames.Fcbh);
            foreach (var bookIdFcbh in myList)
            {
                var bookIdParatext = BookNames.ToParatext(bookIdFcbh);
                bookIdParatext.ShouldNotBeNullOrEmpty();
                output.WriteLine($"{bookIdFcbh} => {bookIdParatext}");
            }
        }

        [Fact]
        public void ShouldGetFcbh()
        {
            var myList = new List<string>(BookNames.Paratext);
            foreach (var bookIdParatext in myList)
            {
                var bookIdFcbh = BookNames.ToFcbh(bookIdParatext);
                bookIdFcbh.ShouldNotBeNullOrEmpty();
                output.WriteLine($"{bookIdParatext} => {bookIdFcbh}");
            }
        }

        [Fact]
        public void ShouldFallBackWhenNotFoundInParatext()
        {
            BookNames.ToParatext("AAA").ShouldBe("AAA");
        }

        [Fact]
        public void ShouldFallBackWhenNotFoundInFcbh()
        {
            BookNames.ToParatext("ZZZ").ShouldBe("ZZZ");
        }
    }
}
