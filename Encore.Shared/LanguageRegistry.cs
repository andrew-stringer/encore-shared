﻿#region Documentation
// file:	Resources\LanguageRegistry.cs
//
// summary:	Implements the language registry class
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Encore.Shared
{
    /// <summary>   Provides access to language registry data from IANA. </summary>
    public class LanguageRegistry : ILanguageRegistry
    {
        #region Documentation
        /// <summary>   Gets or sets the languages. </summary>
        /// <value> The languages. </value>
        #endregion

        public List<LanguageRegistryItem> Languages { get; set; }

        /// <summary>   Default constructor. </summary>
        public LanguageRegistry()
        {
            var registryPath = Path.Combine("Data", "registry.json");
            using (var r = new StreamReader(registryPath))
            {
                var json = r.ReadToEnd();
                Languages = JsonConvert.DeserializeObject<List<LanguageRegistryItem>>(json).Where(x => x.Type == "language").ToList();
            }
        }
    }
}
