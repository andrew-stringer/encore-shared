﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Encore.Shared
{
    public interface IChapterNumbers
    {
        /// <summary>
        /// A dictionary for looking up global chapter numbering.
        /// </summary>
        List<Tuple<string, int, int>> Chapters { get; }
    }
}
