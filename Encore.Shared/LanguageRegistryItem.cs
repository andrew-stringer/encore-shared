﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Encore.Shared
{
    /// <summary>   A language registry item. Definition based on https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry. </summary>
    public class LanguageRegistryItem
    {
        public string Type { get; set; }
        public string Subtag { get; set; }
        public List<string> Description { get; set; }
        public string Added { get; set; }

        [JsonProperty("Suppress-Script")]
        public string SuppressScript { get; set; }
        public string Scope { get; set; }
        public string Macrolanguage { get; set; }
        public string Deprecated { get; set; }

        [JsonProperty("Preferred-Value")]
        public string PreferredValue { get; set; }
        public List<string> Comments { get; set; }
        public List<string> Prefix { get; set; }
        public string Tag { get; set; }
    }
}