﻿using System.Collections.Generic;

namespace Encore.Shared
{
    public interface ILanguageRegistry
    {
        List<LanguageRegistryItem> Languages { get; set; }
    }
}