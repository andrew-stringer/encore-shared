﻿#region Documentation
// file:	Resources\BookNames.cs
//
// summary:	Implements the book names class
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace Encore.Shared
{
    /// <summary>   A book names resource class. </summary>
    public static class BookNames
    {
        #region Documentation
        /// <summary>   Gets the Paratext 3-letter book codes. </summary>
        /// <value> The paratext book codes. </value>
        #endregion

        public static string[] Paratext => new[] {
            "GEN", "EXO", "LEV", "NUM", "DEU", "JOS", "JDG", "RUT", "1SA", "2SA",
            "1KI", "2KI", "1CH", "2CH", "EZR", "NEH", "EST", "JOB", "PSA", "PRO",
            "ECC", "SNG", "ISA", "JER", "LAM", "EZK", "DAN", "HOS", "JOL", "AMO",
            "OBA", "JON", "MIC", "NAM", "HAB", "ZEP", "HAG", "ZEC", "MAL", "MAT",
            "MRK", "LUK", "JHN", "ACT", "ROM", "1CO", "2CO", "GAL", "EPH", "PHP",
            "COL", "1TH", "2TH", "1TI", "2TI", "TIT", "PHM", "HEB", "JAS", "1PE",
            "2PE", "1JN", "2JN", "3JN", "JUD", "REV" };

        #region Documentation
        /// <summary>   Gets the fcbh legacy 3-letter book codes. </summary>
        /// <value> The fcbh legacy book codes. </value>
        #endregion

        public static string[] Fcbh => new[] {
            "GEN", "EXO", "LEV", "NUM", "DEU", "JOS", "JDG", "RUT", "1SM", "2SM",
            "1KI", "2KI", "1CH", "2CH", "EZR", "NEH", "EST", "JOB", "PSM", "PRV",
            "ECC", "SOS", "ISA", "JER", "LAM", "EZE", "DAN", "HOS", "JOE", "AMO",
            "OBA", "JON", "MIC", "NAH", "HAB", "ZEP", "HAG", "ZEC", "MAL", "MAT",
            "MRK", "LUK", "JHN", "ACT", "ROM", "1CO", "2CO", "GAL", "EPH", "PHP",
            "COL", "1TH", "2TH", "1TI", "2TI", "TTS", "PHM", "HEB", "JMS", "1PE",
            "2PE", "1JN", "2JN", "3JN", "JUD", "REV" };

        public static string ToParatext(this string bookId)
        {
            if (Array.Exists(Fcbh, m => m == bookId))
            {
                return Paratext[Array.FindIndex(Fcbh, m => m == bookId)];
            }

            return bookId;
        }

        public static string ToFcbh(this string bookId)
        {
            if (Array.Exists(Paratext, m => m == bookId))
            {
                return Fcbh[Array.FindIndex(Paratext, m => m == bookId)];
            }

            return bookId;
        }            
    }
}
